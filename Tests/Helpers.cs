﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RedFrogs.Tests
{
    public class Helpers
    {
        public static IEnumerable<Assembly> AllAssemblies
        {
            get
            {
                yield return typeof(RedFrogs.CallCentre.WebApiApplication).Assembly;
                yield return typeof(Operations.WebApiApplication).Assembly;
                yield return typeof(RedFrogs.Shared.Domain.UnitOfWork).Assembly;
                yield return typeof(RedFrogs.Shared.Web.Infrastructure.CommonConfig).Assembly;
            }
        }

        public static IEnumerable<Type> AllTypes
        {
            get { return AllAssemblies.SelectMany(a => a.DefinedTypes); }
        }
    }
}
