﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Tests.Conventions
{
    public class FactConventions
    {
        public IEnumerable<TestCaseDataEx> AllFactTypes
        {
            get
            {
                return from t in Helpers.AllTypes
                    where t.Name.EndsWith("Fact") &&
                        t.IsClass &&
                        !t.IsAbstract &&
                        t.Name != "UnitOfWorkFact"
                    select new TestCaseDataEx(t.FullName, t);
            }
        }

        [TestCaseSource("AllFactTypes")]
        public void AllFactsInheritFromFactAbout(Type factType)
        {
            var baseType = factType.BaseType;
            while (baseType != null)
            {
                if (baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(FactAbout<>))
                    return;

                baseType = baseType.BaseType;
            }

            Assert.Fail("Does not inherit from FactAbout<T>");
        }

        [TestCaseSource("AllFactTypes")]
        public void AllFactsHaveTheSerializableAttributeSoThatItCanBeBinaryCopied(Type factType)
        {
            factType.Should().BeDecoratedWith<SerializableAttribute>();
        }

        [TestCaseSource("AllFactTypes")]
        public void AllFactsHaveAParameterlessPublicContructorSoThatItCanBeJsonSerialized(Type factType)
        {
            factType.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null).Should().NotBeNull("No parameterless public constructor");
        }

    }
}