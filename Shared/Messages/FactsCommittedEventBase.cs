﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Shared.Messages
{
    [DataContract]
    public abstract class FactsCommittedEventBase : IBusEvent
    {
        private readonly static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        private IFact[] _facts;


        [DataMember]
        public string[] FactStrings { get; set; }

        public IFact[] Facts
        {
            get
            {
                if (_facts == null)
                    _facts = FactStrings.Select(s => JsonConvert.DeserializeObject<IFact>(s, JsonSerializerSettings)).ToArray();// TODO: Don't do this
                return _facts;
            }
        }

        protected FactsCommittedEventBase(IReadOnlyList<IFact> facts)
        {
            _facts = facts.ToArray();
            FactStrings = facts.Select(f => JsonConvert.SerializeObject(f, JsonSerializerSettings)).ToArray();// TODO: Don't do this
        }
    }
}