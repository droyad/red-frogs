﻿using System;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Shared.Messages
{
    [Serializable]
    public class BroadcastFact<T> : IBusEvent where T : IFact
    {
        public BroadcastFact(T fact)
        {
            Fact = fact;
        }

        public T Fact { get; set; }
    }
}