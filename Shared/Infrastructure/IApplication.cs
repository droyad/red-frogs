﻿using System.Collections.Generic;
using System.Reflection;

namespace RedFrogs.Shared.Infrastructure
{
    public interface IApplication
    {
        string Name { get; }
        IEnumerable<Assembly> HandlerAssemblies { get; }
    }
}