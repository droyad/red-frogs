﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RedFrogs.Shared.Infrastructure
{
    public class InterestingAssemblies
    {
        private static readonly IList<Assembly> _primary = new List<Assembly>();
        private static readonly IList<Assembly> _support = new List<Assembly>();

        public static Assembly[] All
        {
            get { return _primary.Union(_support).ToArray(); }
        }

        public static Assembly[] Primary
        {
            get { return _primary.ToArray(); }
        }


        public static void AddPrimary(Assembly assembly)
        {
            _primary.Add(assembly);
        }

        public static void AddSupport(Assembly assembly)
        {
            _support.Add(assembly);
        }
    }
}