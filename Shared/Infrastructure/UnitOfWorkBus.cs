﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Nimbus;
using Nimbus.MessageContracts;

namespace RedFrogs.Shared.Infrastructure
{
    public interface IUnitOfWorkBus
    {
        void PublishOnCommit<T>(T busEvent) where T : IBusEvent;
        Task Commit();
    }

    [InstancePerLifetimeScope]
    public class UnitOfWorkBus : IUnitOfWorkBus
    {
        private readonly IBus _bus;
        private readonly List<IBusEvent> _toPublish = new List<IBusEvent>();

        public UnitOfWorkBus(IBus bus)
        {
            _bus = bus;
        }

        public void PublishOnCommit<T>(T busEvent) where T : IBusEvent
        {
            var mergeInto = _toPublish.OfType<ICanMerge<T>>().FirstOrDefault();
            if(mergeInto == null)
                _toPublish.Add(busEvent);
            else
                mergeInto.MergeIntoMe(busEvent);
        }

        public async Task Commit()
        {
            var publishTasks = _toPublish.Select(e => (Task) _bus.Publish((dynamic) e));
            await Task.WhenAll(publishTasks);
        }
    }
}