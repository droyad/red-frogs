﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Autofac;
using Microsoft.WindowsAzure.ServiceRuntime;
using Nimbus;
using Nimbus.Configuration;
using Nimbus.Infrastructure;
using Nimbus.InfrastructureContracts;
using Nimbus.Logger.Serilog;
using RedFrogs.Shared.Config;

namespace RedFrogs.Shared.Infrastructure
{
    public class NimbusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var id = RoleEnvironment.IsAvailable
                ? Regex.Replace(RoleEnvironment.CurrentRoleInstance.Id, "[^A-Za-z0-9]", "_")
                : Environment.MachineName + Process.GetCurrentProcess().Id;

            var handlerTypesProvider = new AssemblyScanningTypeProvider(InterestingAssemblies.All);

            builder.RegisterInstance(new SerilogStaticLogger()).AsImplementedInterfaces();
            builder.RegisterNimbus(handlerTypesProvider);
            builder.Register(c => new BusBuilder()
                                 .Configure()
                                 .WithConnectionString(c.Resolve<AzureBusConnectionString>())
                                 .WithNames(c.Resolve<ApplicationName>(), id)
                                 .WithTypesFrom(handlerTypesProvider)
                                 .WithAutofacDefaults(c)
                                 .Build()
                            )
                   .AutoActivate()
                   .As<Bus>()
                   .OnActivated(c => c.Instance.Start())
                   .SingleInstance();
           
            builder.Register(c => new SerilogBusWrapper(c.Resolve<Bus>(), c.Resolve<ILogger>()))
                    .As<IBus>()
                    .SingleInstance();
        }
    }
}