using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nimbus;
using Nimbus.InfrastructureContracts;
using Nimbus.MessageContracts;
using Nimbus.PoisonMessages;

namespace RedFrogs.Shared.Infrastructure
{
    public class SerilogBusWrapper : IBus
    {
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public SerilogBusWrapper(IBus bus, ILogger logger)
        {
            _bus = bus;
            _logger = logger;
        }

        public Task Send<TBusCommand>(TBusCommand busCommand) where TBusCommand : IBusCommand
        {
            _logger.Debug("Sending command {@command}", busCommand);
            return _bus.Send(busCommand);
        }

        public Task Defer<TBusCommand>(TimeSpan delay, TBusCommand busCommand) where TBusCommand : IBusCommand
        {
            _logger.Debug("Deferring command {@command} with a delay of {delay}", busCommand, delay);
            return _bus.Defer(delay, busCommand);
        }

        public Task Defer<TBusCommand>(DateTimeOffset processAt, TBusCommand busCommand) where TBusCommand : IBusCommand
        {
            _logger.Debug("Deferring command {@command} to be processed at {processAt}", busCommand, processAt);
            return _bus.Defer(processAt, busCommand);
        }

        public async Task<TResponse> Request<TRequest, TResponse>(IBusRequest<TRequest, TResponse> busRequest)
            where TRequest : IBusRequest<TRequest, TResponse>
            where TResponse : IBusResponse
        {
            _logger.Debug("Requesting {@request}", busRequest);
            var response = await _bus.Request(busRequest);
            _logger.Debug("Recieved response {@response} to request {@request}", response, busRequest);
            return response;
        }

        public async Task<TResponse> Request<TRequest, TResponse>(IBusRequest<TRequest, TResponse> busRequest, TimeSpan timeout)
            where TRequest : IBusRequest<TRequest, TResponse>
            where TResponse : IBusResponse
        {
            _logger.Debug("Requesting {@request} with timeout of {timeout}", busRequest, timeout);
            var response = await _bus.Request(busRequest, timeout);
            _logger.Debug("Recieved response {@response} to request {@request}", response, busRequest);
            return response;
        }

        public async Task<IEnumerable<TResponse>> MulticastRequest<TRequest, TResponse>(IBusRequest<TRequest, TResponse> busRequest, TimeSpan timeout)
            where TRequest : IBusRequest<TRequest, TResponse>
            where TResponse : IBusResponse
        {
            _logger.Debug("Multicast requesting {@request} with timeout of {timeout}", busRequest, timeout);
            var responses = await _bus.MulticastRequest(busRequest, timeout);
            _logger.Debug("Recieved responses {@responses} to request {@request}", responses, busRequest);
            return responses;
        }

        public Task Publish<TBusEvent>(TBusEvent busEvent) where TBusEvent : IBusEvent
        {
            _logger.Debug("Publishing event {@event}", busEvent);
            return _bus.Publish(busEvent);
        }

        public IDeadLetterQueues DeadLetterQueues
        {
            get { return _bus.DeadLetterQueues; }
        }
    }
}