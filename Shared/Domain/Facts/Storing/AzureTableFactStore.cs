﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Domain.Facts.Storing
{
    public class AzureTableFactStore<T> : IFactStore<T> where T : AggregateRoot
    {
        private readonly Lazy<CloudTable> _table;
        private readonly string _tableName = typeof(T).Name.Replace(".", "").ToLower();

        public AzureTableFactStore(ICloudStorage cloudStorage)
        {
            _table = new Lazy<CloudTable>(() => cloudStorage.GetTable(_tableName));
        }

        public async Task IntroduceFacts(IReadOnlyList<IFact> factsToIntroduce)
        {
            var partitionedFacts = from f in factsToIntroduce
                                   where f.ForType == typeof(T)
                                   select new AzureTableFactWrapper<T>(f)
                                       into wrapped
                                       group wrapped by wrapped.PartitionKey; // All items in batch must have same partition key. Because.

            var batchTasks = partitionedFacts.Select(partition =>
            {
                var batch = new TableBatchOperation();
                foreach (var fact in partition)
                    batch.Insert(fact);
                return _table.Value.ExecuteBatchAsync(batch);
            });

            await Task.WhenAll(batchTasks);
        }

        public async Task<IReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>>> GetFactStream()
        {
            var query = new TableQuery<AzureTableFactWrapper<T>>();
            var dict = (await ExecuteQuery(query))
                .GroupBy(q => q.AggregateId)
                .ToDictionary(g => g.Key, g => (IReadOnlyList<FactAbout<T>>)g.ToArray());
            return new ReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>>(dict);
        }

        public async Task<IReadOnlyList<FactAbout<T>>> GetFactStream(Guid aggregateId)
        {
            var query = new TableQuery<AzureTableFactWrapper<T>>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, aggregateId.ToString()));
            return await ExecuteQuery(query);
        }

        private async Task<IReadOnlyList<FactAbout<T>>> ExecuteQuery(TableQuery<AzureTableFactWrapper<T>> query)
        {
            TableContinuationToken continuationToken = null;
            var results = new List<AzureTableFactWrapper<T>>();
            do
            {
                var queryResult = await _table.Value.ExecuteQuerySegmentedAsync(query, continuationToken);
                continuationToken = queryResult.ContinuationToken;
                results.AddRange(queryResult.Results);
            } while (continuationToken != null);

            return FactAbout<T>.Order(results.Select(r => r.Fact).Cast<FactAbout<T>>());
        }
    }
}