using System;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts.Storing
{
    public class AzureTableFactWrapper<T> : TableEntity where T : AggregateRoot
    {
// ReSharper disable once StaticFieldInGenericType
        private readonly static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        private IFact _fact;

        public AzureTableFactWrapper()
        {
        }

        public AzureTableFactWrapper(IFact fact)
        {
            _fact = fact;
            PartitionKey = fact.AggregateId.ToString();
            RowKey = fact.UnitOfWorkId + "-" + fact.Ordinal;
            Timestamp = fact.Timestamp;
            UnitOfWorkId = fact.UnitOfWorkId;
            Ordinal = fact.Ordinal;
            FactJson = JsonConvert.SerializeObject(fact, JsonSerializerSettings);// TODO: Don't do this
        }

        public int Ordinal { get; set; }
        public Guid UnitOfWorkId { get; set; }
        public string FactJson { get; set; }

        public IFact Fact
        {
            get
            {
                if (_fact == null)
                    _fact = JsonConvert.DeserializeObject<IFact>(FactJson, JsonSerializerSettings); // TODO: Don't do this
                return _fact;
            }
        }
    }
}