﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts.Storing
{

    public interface IFactStore
    {
        Task IntroduceFacts(IReadOnlyList<IFact> factsToIntroduce);
    }

    public interface IFactStore<T> : IFactStore where T : AggregateRoot
    {
        Task<IReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>>> GetFactStream();
        Task<IReadOnlyList<FactAbout<T>>> GetFactStream(Guid aggregateId);
    }
}