using System;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Nimbus;
using Nimbus.InfrastructureContracts;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.Facts.Raising;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Shared.Domain.Facts
{
    public interface ICommittedFactDistributer
    {
        Task Distribute(IFact[] facts);
    }

    public abstract class CommittedFactDistributerBase<T> : ICommittedFactDistributer, IHandleMulticastEvent<T> where T : FactsCommittedEventBase
    {
        private readonly IBus _bus;
        private readonly ILifetimeScope _lifetimeScope;
        private readonly ICommittedFactBroker _committedFactBroker;

        protected CommittedFactDistributerBase(IBus bus, ILifetimeScope lifetimeScope, ICommittedFactBroker committedFactBroker)
        {
            _bus = bus;
            _lifetimeScope = lifetimeScope;
            _committedFactBroker = committedFactBroker;
        }

        public async Task Distribute(IFact[] facts)
        {
            await _bus.Publish(CreateEvent(facts));
        }

        protected abstract T CreateEvent(IFact[] facts);

        public void Handle(T busEvent)
        {
            HandleInternal(busEvent.Facts).Wait();
        }

        private async Task HandleInternal(IFact[] facts)
        {
            var invalidateTasks = facts.GroupBy(f => f.ForType).Select(g => Invalidate(g.Key, g.ToArray()));
            await Task.WhenAll(invalidateTasks.ToArray());

            foreach (var fact in facts)
                await _committedFactBroker.Raise((dynamic) fact); // TODO: Should we start a new lifetime scope?
        }

        private async Task Invalidate(Type type, IFact[] facts)
        {
            var aggregateStoreType = typeof(ISnapshot<>).MakeGenericType(type);
            var aggregateStore = _lifetimeScope.Resolve(aggregateStoreType) as ISnapshot; // TODO: Make this better
            if (aggregateStore != null)
                await aggregateStore.Invalidate(facts.Select(f => f.AggregateId).ToArray());
        }
    }
}