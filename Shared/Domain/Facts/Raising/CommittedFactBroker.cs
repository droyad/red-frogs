﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;

namespace RedFrogs.Shared.Domain.Facts.Raising
{
    public interface ICommittedFactBroker
    {
        Task Raise<T>(T fact) where T : IFact;
    }

    public class CommittedFactBroker : ICommittedFactBroker
    {
        private readonly ILifetimeScope _scope;

        public CommittedFactBroker(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public async Task Raise<T>(T fact) where T : IFact
        {
            var handlers = _scope.Resolve<IEnumerable<ICareAboutCommittedFact<T>>>();
            foreach (var handler in handlers) 
                await handler.Handle(fact);
        }
    }
}