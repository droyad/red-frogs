﻿using System.Threading.Tasks;

namespace RedFrogs.Shared.Domain.Facts.Raising
{
    public interface ICareAboutUncommittedFact<in T> : ICareAboutTheFacts
    {
        Task Handle(T fact);
    }
}