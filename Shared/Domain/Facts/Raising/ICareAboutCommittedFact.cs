﻿using System.Threading.Tasks;

namespace RedFrogs.Shared.Domain.Facts.Raising
{
    public interface ICareAboutCommittedFact<in T> : ICareAboutTheFacts
    {
        Task Handle(T fact);
    }
}