﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;

namespace RedFrogs.Shared.Domain.Facts.Raising
{
    public interface IUncommittedFactBroker
    {
        Task Raise<T>(T fact) where T : IFact;
    }

    public class UncommittedFactBroker : IUncommittedFactBroker
    {
        private readonly ILifetimeScope _scope;

        public UncommittedFactBroker(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public async Task Raise<T>(T fact) where T : IFact
        {
            var handlers = _scope.Resolve<IEnumerable<ICareAboutUncommittedFact<T>>>();
            foreach (var handler in handlers) 
                await handler.Handle(fact);
        }
    }
}