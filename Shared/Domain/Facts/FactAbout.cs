﻿using System;
using System.Collections.Generic;
using System.Linq;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts
{
    public interface IFact
    {
        Guid AggregateId { get; }
        Type ForType { get; }
        Guid UnitOfWorkId { get; }
        DateTimeOffset Timestamp { get;  }
        int Ordinal { get;  }
        void SetStream(Guid unitOfWorkId, DateTimeOffset timestamp, int ordinal);
    }

    [Serializable]
    public abstract class FactAbout<T> : IFact where T : AggregateRoot
    {
        protected FactAbout(T aggregateRoot)
        {
            AggregateId = aggregateRoot.Id;
        }

        protected FactAbout()
        {
        }

        public Guid AggregateId { get; set; }
        
        public Guid UnitOfWorkId { get; private set; }
        public DateTimeOffset Timestamp { get; private set; }
        public int Ordinal { get; private set; }

        public Type ForType
        {
            get { return typeof (T); }
        }

        public void SetStream(Guid unitOfWorkId, DateTimeOffset timestamp, int ordinal)
        {
            UnitOfWorkId = unitOfWorkId;
            Timestamp = Timestamp;
            Ordinal = ordinal;
        }

        public static IReadOnlyList<FactAbout<T>> Order(IEnumerable<FactAbout<T>> facts)
        {
            return facts
                .ToArray()
                .OrderBy(f => f.Timestamp)
                .ThenBy(f => f.UnitOfWorkId)
                .ThenBy(f => f.Ordinal)
                .ToArray();
        }

    }
}