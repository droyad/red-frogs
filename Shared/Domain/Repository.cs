﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain
{
    public interface IRepository<T> where T : AggregateRoot
    {
        Task<T> Get(Guid id);
        void Add(T item);
        Task<IReadOnlyList<T>> All();
        Task CommitUnitOfWork();
        Task<IReadOnlyList<T>> Filter(Func<T, bool> filter);
        Task<IReadOnlyList<T>> Filter(Func<IEnumerable<T>, IEnumerable<T>> filter);
        Task<TReturn> Query<TReturn>(Func<IEnumerable<T>, TReturn> query);
    }

    /// <summary>
    /// This repository ensures that the same instance of T is returned for the same id in the same lifetimescope
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : AggregateRoot
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISnapshot<T> _snapshot;
        private readonly Dictionary<Guid, T> _alreadyReturned = new Dictionary<Guid, T>();

        public Repository(IUnitOfWork unitOfWork, ISnapshot<T> snapshot)
        {
            _unitOfWork = unitOfWork;
            _snapshot = snapshot;
        }

        public async Task<T> Get(Guid id)
        {
            T item;
            if (_alreadyReturned.TryGetValue(id, out item))
                return item;

            item = await _snapshot.Get(id);
            _alreadyReturned.Add(id, item);

            if (item == null)
                return null;

            _unitOfWork.EnlistInTransaction(item);
            
            return item;
        }

        public void Add(T item)
        {
            _unitOfWork.EnlistInTransaction(item);
            _alreadyReturned.Add(item.Id, item);
        }

        public async Task<IReadOnlyList<T>> All()
        {
            var items = (await _snapshot.All()).ToArray();

            for (var x = 0; x < items.Length; x++)
            {
                var item = items[x];
                T alreadyReturned;
                if (_alreadyReturned.TryGetValue(item.Id, out alreadyReturned))
                {
                    items[x] = alreadyReturned;
                }
                else
                {
                    _alreadyReturned.Add(item.Id, item);
                    _unitOfWork.EnlistInTransaction(item);
                }
            }

            return items;
        }

        public async Task CommitUnitOfWork()
        {
            await _unitOfWork.Commit();
        }

        public async Task<IReadOnlyList<T>> Filter(Func<T, bool> filter)
        {
            var result = await _snapshot.Filter(filter);
            _unitOfWork.EnlistInTransaction(result);
            return result;

        }

        public async Task<IReadOnlyList<T>> Filter(Func<IEnumerable<T>, IEnumerable<T>> filter)
        {
            var result = await _snapshot.Filter(filter);
            _unitOfWork.EnlistInTransaction(result);
            return result;
        }

        /// <summary>
        /// Does not return entities of the same reference
        /// </summary>
        /// <typeparam name="TReturn"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<TReturn> Query<TReturn>(Func<IEnumerable<T>, TReturn> query)
        {
            return _snapshot.Query(query);
        }
    }
}