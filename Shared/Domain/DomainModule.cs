﻿using Autofac;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.Facts.Raising;
using RedFrogs.Shared.Domain.Facts.Storing;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof (AzureTableFactStore<>))
                .As(typeof (IFactStore<>))
                .SingleInstance();

            builder.RegisterGeneric(typeof (Snapshot<>))
                .As(typeof (ISnapshot<>))
                .SingleInstance();

            builder.RegisterGeneric(typeof (AggregateRebuilder<>))
                .As(typeof (IAggregateRebuilder<>))
                .SingleInstance();

            builder.RegisterGeneric(typeof (Repository<>))
                .As(typeof (IRepository<>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(InterestingAssemblies.Primary)
                .Where(t => t.IsClass && t.IsAssignableTo<ICareAboutTheFacts>())
                .InstancePerDependency()
                .AsImplementedInterfaces();

            builder.RegisterType<UncommittedFactBroker>()
                .As<IUncommittedFactBroker>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CommittedFactBroker>()
                .As<ICommittedFactBroker>()
                .InstancePerLifetimeScope();
        }
    }
}