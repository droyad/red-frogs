﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedFrogs.Shared.Extensions;

namespace RedFrogs.Shared.Domain.Aggregates
{
    public interface ISnapshot
    {
        Task Invalidate(Guid[] ids); // TODO, move this into generic interface
    }

    public interface ISnapshot<T> : ISnapshot where T : AggregateRoot
    {
        Task<T> Get(Guid id);
        Task<IReadOnlyList<T>> All();
        Task<TReturn> Query<TReturn>(Func<IEnumerable<T>, TReturn> query);
        Task<IReadOnlyList<T>>  Filter(Func<T, bool> filter);
        Task<IReadOnlyList<T>> Filter(Func<IEnumerable<T>, IEnumerable<T>> filter);
    }

    /// <summary>
    /// The snapshot ensures that a new instance of each entity is returned on every request so that
    /// the snapshot cannot be modified outside of introducing new facts
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Snapshot<T> : ISnapshot<T> where T : AggregateRoot
    {
        private readonly IAggregateRebuilder<T> _aggregateRebuilder;
        private readonly AsyncLazy<ConcurrentDictionary<Guid, T>> _items;

        public Snapshot(IAggregateRebuilder<T> aggregateRebuilder)
        {
            _aggregateRebuilder = aggregateRebuilder;
            _items = new AsyncLazy<ConcurrentDictionary<Guid, T>>(LoadAll);
        }

        private async Task<ConcurrentDictionary<Guid, T>> LoadAll()
        {
            var items = await _aggregateRebuilder.RebuildAll();
            return new ConcurrentDictionary<Guid, T>( items.ToDictionary(a => a.Id));
        }

        public async Task<T> Get(Guid id)
        {
            if (!_items.IsValueCreated)
                return await _aggregateRebuilder.Rebuild(id);

            T result;
            var items = await _items.Value;
            return items.TryGetValue(id, out result)
                ? result.BinaryClone()
                : null;
        }

        public async Task<IReadOnlyList<T>> All()
        {
            var items = await _items.Value;
            return items.Values.Select(a => a.BinaryClone()).ToArray();
        }

        public async Task<TReturn> Query<TReturn>(Func<IEnumerable<T>, TReturn> query)
        {
            var items = await _items.Value;
            return query(items.Values);
        }

        public async Task<IReadOnlyList<T>> Filter(Func<T, bool> filter)
        {
            var items = await _items.Value;
            return items.Values.Where(filter).Select(f => f.BinaryClone()).ToArray();
        }

        public async Task<IReadOnlyList<T>> Filter(Func<IEnumerable<T>, IEnumerable<T>> filter)
        {
            var items = await _items.Value;
            return filter(items.Values).Select(t => t.BinaryClone()).ToArray();
        }

        public async Task Invalidate(Guid[] ids)
        {
            if (!_items.IsValueCreated)
                return;

            var items = await _items.Value;
            foreach (var id in ids)
            {
                var rebuilt = await _aggregateRebuilder.Rebuild(id);
                items.AddOrUpdate(id, i => rebuilt, (i, e) => rebuilt);
            }
        }
    }

}