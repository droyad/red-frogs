﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Domain.Facts.Storing;

namespace RedFrogs.Shared.Domain.Aggregates
{
    public interface IAggregateRebuilder<T> where T : AggregateRoot
    {
        Task<IReadOnlyList<T>> RebuildAll();
        Task<T> Rebuild(Guid id);
    }

    public class AggregateRebuilder<T> : IAggregateRebuilder<T> where T : AggregateRoot
    {
        private readonly IFactStore<T> _factStore;

        public AggregateRebuilder(IFactStore<T> factStore)
        {
            _factStore = factStore;
        }

        public async Task<IReadOnlyList<T>> RebuildAll()
        {
            var facts = await _factStore.GetFactStream();
            return facts
                .Select(stream => Rebuild(stream.Value))
                .ToArray();
        }

        public async Task<T> Rebuild(Guid aggregateId)
        {
            var facts = await _factStore.GetFactStream(aggregateId);
            return Rebuild(facts);
        }

        private static T Rebuild(IReadOnlyList<FactAbout<T>> stream)
        {
            var aggregateRoot = (AggregateRoot)Activator.CreateInstance(typeof(T), true);
            foreach (var fact in stream)
                ((dynamic)aggregateRoot).Apply((dynamic)fact);
            return (T) aggregateRoot;
        }

    }
}