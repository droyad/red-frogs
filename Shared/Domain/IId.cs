using System;

namespace RedFrogs.Shared.Domain
{
    public interface IId
    {
        Guid Id { get; }
    }
}