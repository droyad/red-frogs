﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Domain.Facts.Raising;
using RedFrogs.Shared.Domain.Facts.Storing;
using RedFrogs.Shared.Extensions;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Domain
{
    public interface IUnitOfWork
    {
        void EnlistInTransaction(AggregateRoot item);
        Task Commit();
        void EnlistInTransaction(IEnumerable<AggregateRoot> items);
    }

    [InstancePerLifetimeScope]
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ILifetimeScope _lifetimeScope;
        private readonly IUncommittedFactBroker _uncommittedFactBroker;
        private readonly IUnitOfWorkBus _unitOfWorkBus;
        private readonly ICommittedFactDistributer _committedFactDistributer;
        private readonly HashSet<AggregateRoot> _aggregateRootsInTransaction = new HashSet<AggregateRoot>();
        private readonly Guid _unitOfWorkId = Guid.NewGuid();

        public UnitOfWork(ILifetimeScope lifetimeScope, IUncommittedFactBroker uncommittedFactBroker, IUnitOfWorkBus unitOfWorkBus, ICommittedFactDistributer committedFactDistributer)
        {
            _lifetimeScope = lifetimeScope;
            _uncommittedFactBroker = uncommittedFactBroker;
            _unitOfWorkBus = unitOfWorkBus;
            _committedFactDistributer = committedFactDistributer;
        }

        public void EnlistInTransaction(AggregateRoot item)
        {
            _aggregateRootsInTransaction.Add(item);
        }

        public void EnlistInTransaction(IEnumerable<AggregateRoot> items)
        {
            foreach(var item in items)
                _aggregateRootsInTransaction.Add(item);
        }
        public async Task Commit()
        {
            var facts = (await FactFindAndRaiseAllTheFacts()).ToArray();
            if (facts.Any())
            {
                var now = DateTimeOffset.Now;
                for(var x = 0; x < facts.Length; x++)
                    facts[x].SetStream(_unitOfWorkId, now, x);

                await SendFactsToTheStore(facts);
                await _committedFactDistributer.Distribute(facts);
            }
            await _unitOfWorkBus.Commit();
        }

      

        private async Task SendFactsToTheStore(IFact[] facts)
        {
            var storeTasks = facts
                .Select(f => f.ForType)
                .Distinct()
                .Select(async type =>
                {
                    var storeType = typeof (IFactStore<>).MakeGenericType(type);
                    var store = (IFactStore) _lifetimeScope.Resolve(storeType);
                    await store.IntroduceFacts(facts);
                });
            await Task.WhenAll(storeTasks);
        }

        private async Task<IFact[]> FactFindAndRaiseAllTheFacts()
        {
            var allFacts = new List<IFact>();
            while (true)
            {
                var factsInThisPass = _aggregateRootsInTransaction
                        .SelectMany(item => item.GetAndClearFacts())
                        .ToArray();

                if (factsInThisPass.None())
                    return allFacts.ToArray();

                allFacts.AddRange(factsInThisPass);

                foreach (var fact in factsInThisPass)
                    await _uncommittedFactBroker.Raise((dynamic) fact);
            }
        }

    }
}