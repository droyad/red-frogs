﻿using System;
using System.Collections.Generic;

namespace RedFrogs.Shared.Extensions
{
    public static class StringExtensions
    {
        public static string FormatWith(this string str, params object[] args)
        {
            return string.Format(str, args);
        }

        public static string ToCsv(this IEnumerable<string> values)
        {
            return string.Join(", ", values);
        }

        public static string ToCsv(this IEnumerable<object> values)
        {
            return string.Join(", ", values);
        }

        public static string JoinWith(this IEnumerable<object> values, string joiner)
        {
            return string.Join(joiner, values);
        }

        public static string ToNewLineDelimited(this IEnumerable<string> values)
        {
            return string.Join(Environment.NewLine, values);
        }

        public static string ToNewLineDelimited(this IEnumerable<object> values)
        {
            return string.Join(Environment.NewLine, values);
        }

    }
}