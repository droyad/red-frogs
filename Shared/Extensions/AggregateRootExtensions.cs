﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RedFrogs.Shared.Extensions
{
    public static class AggregateRootExtensions
    {
        public static T BinaryClone<T>(this T entity)
        {
            var serializer = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, entity);
                ms.Position = 0;
                var clone = (T) serializer.Deserialize(ms);
                return clone;
            }
        }
    }
}