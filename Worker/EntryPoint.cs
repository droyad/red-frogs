using System.Net;
using System.Threading;
using Microsoft.WindowsAzure.ServiceRuntime;
using RedFrogs.Shared.Config;
using RedFrogs.Shared.Infrastructure;

namespace Worker
{
    public class EntryPoint : RoleEntryPoint
    {

        public static void Main()
        {
            var ep = new EntryPoint();
            ep.OnStart();
            ep.Run();
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            InterestingAssemblies.AddPrimary(typeof(EndPoint).Assembly);
            InterestingAssemblies.AddSupport(typeof(ConfigModule).Assembly);

//            var builder = new ContainerBuilder();
//            builder.RegisterAssemblyModules(InterestingAssemblies.All);
//            builder.Build();

            return base.OnStart();
        }

        public override void Run()
        {
            Thread.Sleep(Timeout.Infinite); // Lazy
        }
    }
}
