﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using RedFrogs.Operations.Domain.Dispatches.Facts;
using RedFrogs.Shared.Domain.Facts.Raising;

namespace RedFrogs.Operations.Features.DispatchDashboard.FactCarers.WhenADispatchJobsStatusChanges
{
    public class ThenTheBrowsersAreAskedToRefresh : ICareAboutCommittedFact<DispatchJobDispatchedFact>, ICareAboutCommittedFact<DispatchJobReadiedFact>
    {
        public Task Handle(DispatchJobDispatchedFact fact)
        {
            return Task.Run(() => GlobalHost.ConnectionManager.GetHubContext<DispatchDashboardHub>().Clients.All.pleaseRefresh());
        }

        public Task Handle(DispatchJobReadiedFact fact)
        {
            return Task.Run(() => GlobalHost.ConnectionManager.GetHubContext<DispatchDashboardHub>().Clients.All.pleaseRefresh());
        }
    }
}