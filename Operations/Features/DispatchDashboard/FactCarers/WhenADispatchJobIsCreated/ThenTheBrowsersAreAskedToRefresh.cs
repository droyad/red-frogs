﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using RedFrogs.Operations.Domain.Dispatches.Facts;
using RedFrogs.Shared.Domain.Facts.Raising;

namespace RedFrogs.Operations.Features.DispatchDashboard.FactCarers.WhenADispatchJobIsCreated
{
    public class ThenTheBrowsersAreAskedToRefresh : ICareAboutCommittedFact<DispatchJobCreatedFact>
    {
        public Task Handle(DispatchJobCreatedFact fact)
        {
            return Task.Run(() => GlobalHost.ConnectionManager.GetHubContext<DispatchDashboardHub>().Clients.All.pleaseRefresh());
        }
    }
}