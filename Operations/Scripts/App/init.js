$(document).ajaxError(function (event, req) {
    if (req && req.responseText) {
        var data;
        try {
            data = JSON.parse(req.responseText);
            if (data && data.ErrorMessage) {
                toastr.error(data.ErrorMessage);
                return;
            }
        } catch (e) { }
    }
    toastr.error('A server or network-related error occurred');
});