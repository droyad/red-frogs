define('Home.IndexViewModel', [], function () {
    return function () {
        var self = this;

        self.hub = $.connection.dispatchDashboardHub;
        self.hub.client.pleaseRefresh = function () {
            self.getDispatchJobSummary();
        };

        self.dispatchJobSummary = ko.observable();

        self.getDispatchJobSummary = function () {
            $.get('/api/DispatchJobSummary')
                .success(self.dispatchJobSummary);
        };

        self.dispatchNext100Jobs = function () {
            self.dispatchNext(100);
        };

        self.dispatchNext = function(n) {
            $.post('/api/ReadyDispatchJobs?cnt=' + n)
                .success(function(n) {
                    toastr.success(n + " jobs readied for dispatch");
                });
        };

        self.dispatchNext1000Jobs = function () {
            self.dispatchNext(1000);

        };

        self.dispatchAllTheJobs = function () {
            self.dispatchNext(1000000000);

        };

        $.connection.hub.start();
        self.getDispatchJobSummary();
    };
});