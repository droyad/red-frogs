﻿using System.Web.Mvc;

namespace RedFrogs.Operations.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
