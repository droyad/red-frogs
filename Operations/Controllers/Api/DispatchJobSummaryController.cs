﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using RedFrogs.Operations.Domain.Dispatches;
using RedFrogs.Shared.Domain;

namespace RedFrogs.Operations.Controllers.Api
{
    public class DispatchJobSummaryController : ApiController
    {
        private readonly IRepository<DispatchJob> _dispatchJobRepository;

        public DispatchJobSummaryController(IRepository<DispatchJob>  dispatchJobRepository)
        {
            _dispatchJobRepository = dispatchJobRepository;
        }

        public async Task<object> Get()
        {
            return await _dispatchJobRepository.Query(
                jobs =>
                {
                    var q = from j in jobs
                        group j by j.Status
                        into g
                        select new
                        {
                            Status = g.Key,
                            Count = g.Count()
                        };
                    var grouped = q.ToArray();
                    return new
                    {
                        Pending = grouped.Where(g => g.Status == DispatchStatus.Pending).Select(g => g.Count).FirstOrDefault(),
                        ReadyForDispatch = grouped.Where(g => g.Status == DispatchStatus.ReadyForDispatch).Select(g => g.Count).FirstOrDefault(),
                        Dispatched = grouped.Where(g => g.Status == DispatchStatus.Dispatched).Select(g => g.Count).FirstOrDefault(),
                    };
                }
            );
        }
    }
}