using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using RedFrogs.Operations.Domain.Dispatches;
using RedFrogs.Shared.Domain;

namespace RedFrogs.Operations.Controllers.Api
{
    public class ReadyDispatchJobsController : ApiController
    {
        private readonly IRepository<DispatchJob> _dispatchJobRepository;

        public ReadyDispatchJobsController(IRepository<DispatchJob> dispatchJobRepository)
        {
            _dispatchJobRepository = dispatchJobRepository;
        }

        public async Task<int> Post(int cnt)
        {
            var jobs = await _dispatchJobRepository.Filter(j => j.Where(g => g.Status == DispatchStatus.Pending).Take(cnt));

            foreach(var job in jobs)
                job.Ready();
            await _dispatchJobRepository.CommitUnitOfWork();
            return jobs.Count;
        }
    }
}