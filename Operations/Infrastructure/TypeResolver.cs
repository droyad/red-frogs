﻿using System;
using Autofac;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Operations.Infrastructure
{
    [SingleInstance]
    public class TypeResolver : ITypeResolver
    {
        public Type Resolve(string name)
        {
            return Type.GetType(name);
        }
    }
}