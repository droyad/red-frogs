﻿using Microsoft.Owin;
using Owin;
using RedFrogs.Operations.Infrastructure;

[assembly: OwinStartup(typeof(Startup))]
namespace RedFrogs.Operations.Infrastructure
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }

}