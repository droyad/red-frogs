﻿using Autofac;
using Nimbus;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Domain.Facts.Raising;

namespace RedFrogs.Operations.Infrastructure.Facts
{
    public class CommittedFactDistributer : CommittedFactDistributerBase<OperationsFactsCommittedEvent>
    {
        public CommittedFactDistributer(IBus bus, ILifetimeScope lifetimeScope, ICommittedFactBroker committedFactBroker)
            : base(bus, lifetimeScope, committedFactBroker) // TODO: Make this betterer
        {
        }

        protected override OperationsFactsCommittedEvent CreateEvent(IFact[] facts)
        {
            return new OperationsFactsCommittedEvent(facts);
        }
    }
}