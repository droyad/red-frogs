﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Operations.Infrastructure.Facts
{
    [DataContract]
    public class OperationsFactsCommittedEvent : FactsCommittedEventBase
    {
        public OperationsFactsCommittedEvent(IReadOnlyList<IFact> facts) : base(facts)
        {
        }
    }
}