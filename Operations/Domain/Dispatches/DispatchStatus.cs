﻿namespace RedFrogs.Operations.Domain.Dispatches
{
    public enum DispatchStatus
    {
        Pending,
        ReadyForDispatch,
        Dispatched,
        Completed
    }
}