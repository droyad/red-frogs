﻿using System;
using RedFrogs.Operations.Domain.Dispatches.Facts;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Operations.Domain.Dispatches
{
    [Serializable]
    public class DispatchJob : AggregateRoot
    {
        private DispatchJob()
        {
            
        }

        public static DispatchJob Create(Guid jobId)
        {
            var dispatch = new DispatchJob();
            var fact = new DispatchJobCreatedFact(dispatch, jobId);
            dispatch.Append(fact);
            dispatch.Apply(fact);
            return dispatch;
        }


        public void Apply(DispatchJobCreatedFact fact)
        {
            Id = fact.AggregateId;
            JobId = fact.JobId;
        }

        public DispatchStatus Status { get; private set; }
        public Guid JobId { get; private set; }

        public void Ready()
        {
            if (Status != DispatchStatus.Pending)
                throw new InvalidOperationException("Cannot ready a dispatch that is not pending");

            var fact = new DispatchJobReadiedFact(this);
            Append(fact);
            Apply(fact);
        }

        public void Apply(DispatchJobReadiedFact fact)
        {
            Status = DispatchStatus.ReadyForDispatch;
        }

        public void Dispatch()
        {
            if(Status != DispatchStatus.ReadyForDispatch)
                throw new InvalidOperationException("Cannot dispatch a dispatch that is not ready");

            var fact = new DispatchJobDispatchedFact(this);
            Append(fact);
            Apply(fact);
        }



        public void Apply(DispatchJobDispatchedFact fact)
        {
            Status = DispatchStatus.Dispatched;
        }
    }
}