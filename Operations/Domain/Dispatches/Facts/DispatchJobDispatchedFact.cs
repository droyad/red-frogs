﻿using System;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Operations.Domain.Dispatches.Facts
{
    [Serializable]
    public class DispatchJobDispatchedFact : FactAbout<DispatchJob>
    {
        public DispatchJobDispatchedFact()
        {
            
        }

        public DispatchJobDispatchedFact(DispatchJob aggregateRoot) : base(aggregateRoot)
        {
        }
    }
}