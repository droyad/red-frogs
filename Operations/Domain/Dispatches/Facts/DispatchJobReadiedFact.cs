﻿using System;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Operations.Domain.Dispatches.Facts
{
    [Serializable]
    public class DispatchJobReadiedFact : FactAbout<DispatchJob>
    {
        public DispatchJobReadiedFact()
        {

        }

        public DispatchJobReadiedFact(DispatchJob aggregateRoot)
            : base(aggregateRoot)
        {
        }
    }
}