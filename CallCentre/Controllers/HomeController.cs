﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nimbus;
using RedFrogs.Shared.Messages;

namespace RedFrogs.CallCentre.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBus _bus;

        public HomeController(IBus bus)
        {
            _bus = bus;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> ConfirmJob(int cnt)
        {
            var tasks = Enumerable.Range(0, cnt).Select(_ => _bus.Publish(new JobConfirmedEvent() {JobId = Guid.NewGuid()}));
            await Task.WhenAll(tasks);
            return RedirectToAction("Index");
        }
    }

}
