define('Home.IndexViewModel', [], function() {
    return function() {
        var self = this;

        self.dispatchJobs = ko.observableArray([]);

        self.getDispatchJobs = function() {
            $.get('/api/DispatchJobs')
                .success(self.dispatchJobs);
        };

        self.getDispatchJobs();
    };
});